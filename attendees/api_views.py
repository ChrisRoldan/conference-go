from django.http import JsonResponse
import json
from .models import Attendee
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from events.models import Conference

class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name", "email"]

    def get_extra_data(self, o):
        return {"href": o.get_api_url()}

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":

        attendees = Attendee.objects.filter(conference=conference_id)
        return JsonResponse({"attendees": attendees}, encoder=AttendeeListEncoder, safe=False)
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = ["name", "email", "company_name", "created", "conference"]

    def get_extra_data(self, o):
        return {
            "conference": {
                "name": o.conference.name,
                "href": o.conference.get_api_url(),
            }
        }

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(attendee, encoder=AttendeeDetailEncoder, safe=False)

    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:  # request.method == "PUT"
        content = json.loads(request.body)
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(attendee, encoder=AttendeeDetailEncoder, safe=False)
