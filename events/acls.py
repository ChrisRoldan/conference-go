import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={city}+{state}&per_page=1&page=1"
    response = requests.get(url, headers=headers)
    data = response.json()
    return data['photos'][0]['src']['large'] if data['photos'] else None

def get_weather_data(city,state):
    url="https://api.openweathermap.org/geo/1.0/direct"
    params= {
        "q": f"{city}, {state}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
             }
    response= requests.get(url, params=params)
    content = response.json()
    try:
        latitude= content[0]["lat"]
        longitude= content[0]["lon"]
    except (KeyError, IndexError):
        return None
    params= {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url="https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = response.json()
    return {
        "description": content["weather"][0]["description"],
        "temperature": content["main"]["temp"]
    }
